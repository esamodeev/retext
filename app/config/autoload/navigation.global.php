<?php
return array(
        'navigation' => array(
                'account' => array(                                              
                        'account' => array(
                                'label' => 'Account',
                                'route' => 'home',
                                'pages' => array(
                                        'changeemail' => array(
                                                'label' => 'Change Email',
                                                'route' => 'zfcuser/changeemail',
                                        ),
                                        'changepassword' => array(
                                                'label' => 'Change Password',
                                                'route' => 'zfcuser/changepassword',
                                        ),
                                        
                                        'logout' => array(
                                                'label' => 'Sign Out',
                                                'route' => 'zfcuser/logout',
                                        ),
                                ),
                        ),

                ),
                'default' => array(
                        array(
                                'label' => 'Home',
                                'route' => 'home',
                                'pages' => array(
                                        array(
                                                'label' => 'Add new keyword',
                                                'route' => 'retext',
                                                'action' => 'add',
                                        ),
                                        array(
                                                'label' => 'Edit keyword',
                                                'route' => 'retext',
                                                'action' => 'edit',
                                        ),
                                        array(
                                                'label' => 'Delete keyword',
                                                'route' => 'retext',
                                                'action' => 'delete',
                                        ),
                                        'changeemail' => array(
                                                'label' => 'Change Email',
                                                'route' => 'zfcuser/changeemail',
                                        ),
                                        'changepassword' => array(
                                                'label' => 'Change Password',
                                                'route' => 'zfcuser/changepassword',
                                        ),
                                        
                                        'logout' => array(
                                                'label' => 'Sign Out',
                                                'route' => 'zfcuser/logout',
                                        ),                                        
                                ),
                        ),
                ),                
        ),  
);