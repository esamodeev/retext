<?php
namespace Retext;

 use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
 use Zend\ModuleManager\Feature\ConfigProviderInterface;
 use Retext\Model\Retext;
 use Retext\Model\RetextTable;
 use Retext\Controller\RetextController;
 use Zend\Db\ResultSet\ResultSet;
 use Zend\Db\TableGateway\TableGateway;
 
 use Zend\EventManager\StaticEventManager;

class Module implements AutoloaderProviderInterface, ConfigProviderInterface
{
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
         return array(
             'Zend\Loader\ClassMapAutoloader' => array(
                 __DIR__ . '/autoload_classmap.php',
             ),
             'Zend\Loader\StandardAutoloader' => array(
                 'namespaces' => array(
                     __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                 ),
             ),
         );
    }
    
    public function getServiceConfig()
    {
        return array();
    }
    
    public function onBootstrap(\Zend\Mvc\MvcEvent $e){

        $application = $e->getApplication();
        $serviceManager = $application->getServiceManager();
        
        $eventManager = StaticEventManager::getInstance();
        
        $eventManager->attach('Twilio\Controller\CallbackController', 'twilioSmsRequest', function($e) use($serviceManager){
            $entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
            $params = $e->getParams();
            $requestKeyword = trim($params['smsRequest']->getBody());           
            $records = $entityManager->getRepository('Retext\Entity\Retext')->findBy(array('keyword' => $requestKeyword));
            $responseText = count($records) > 0 ? $records[0]->response : 'Invalid keyword';
            $params['smsResponse']->addMessage($responseText);
        });
    }
}
