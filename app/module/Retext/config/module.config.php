<?php
namespace Retext;

return array(
        'controllers' => array(
                'invokables' => array(
                        'Retext\Controller\Retext' => 'Retext\Controller\RetextController'
                ),
        ),
        'view_manager' => array(
                'template_path_stack' => array(
                        'retext' => __DIR__ . '/../view',
                ),
        ),
        'doctrine' => array(
                'driver' => array(
                        __NAMESPACE__ . '_driver' => array(
                                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                                'cache' => 'array',
                                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity')
                        ),
                        'orm_default' => array(
                                'drivers' => array(
                                        __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                                )
                        )
                )
        ),
        'router' => array(
                'routes' => array(
                        'retext' => array(
                                'type'    => 'segment',
                                'options' => array(
                                        'route'    => '/retext[/:action][/:id]',
                                        'constraints' => array(
                                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                                'id'     => '[0-9]+',
                                        ),
                                        'defaults' => array(
                                                'controller' => 'Retext\Controller\Retext',
                                                'action'     => 'index',
                                        ),
                                ),
                                'may_terminate' => true,
                                'child_routes' => array(
                                        'test' => array(
                                                'type' => 'Literal',
                                                'options' => array(
                                                        'route' => '/test',
                                                        'defaults' => array(
                                                                'controller' => 'Retext\Controller\Retext',
                                                                'action'     => 'test',
                                                        ),
                                                ),
                                        ),
                                ),                                
                        ),
                ),
        ),        
);