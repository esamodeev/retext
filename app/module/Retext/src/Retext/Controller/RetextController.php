<?php
namespace Retext\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use Retext\Entity\Retext;
use Retext\Form\RetextForm;
use Doctrine\ORM\EntityManager;

class RetextController extends AbstractActionController
{
    protected $_em;
    
    public function setEntityManager(EntityManager $em) {
        $this->_em = $em;
    }
    
    public function getEntityManager()
    {
        if (null === $this->_em) {
            $this->_em = $this->getServiceLocator()->get('doctrine.entitymanager.orm_default');
        }
        return $this->_em;
    }
    
    public function indexAction()
    {
        return new ViewModel(array(
                'records' => $this->getEntityManager()->getRepository('Retext\Entity\Retext')->findAll(),
        ));
    }
    
    public function addAction()
    {
        $this->_checkStatus();
        
        $form = new RetextForm($this->getEntityManager());
        
        $form->get('submit')->setValue('Add');
                  
        $request = $this->getRequest();
        if ($request->isPost()) {
            $record = new Retext($this->getEntityManager());
            $form->setInputFilter($record->getInputFilter());
            $form->setData($request->getPost());
    
            if ($form->isValid()) {
                $record->exchangeArray($form->getData());
                $this->getEntityManager()->persist($record);
                $this->getEntityManager()->flush();
    
                $this->flashMessenger()->addMessage('Record added.');
                
                // Redirect to list of records
                return $this->redirect()->toRoute('retext');
            }
        }
        return array('form' => $form);
    }
    
    public function editAction()
    {
        $this->_checkStatus();
        
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('retext', array(
                    'action' => 'add'
            ));
        }
    
        $record = $this->getEntityManager()->find('Retext\Entity\Retext', $id);
        if (!$record) {
            return $this->redirect()->toRoute('retext', array(
                    'action' => 'index'
            ));
        }
    
        $form  = new RetextForm($this->getEntityManager());
        $form->bind($record);
        $form->get('submit')->setAttribute('value', 'Save');
           
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setInputFilter($record->getInputFilter());
            $form->setData($request->getPost());
    
            if ($form->isValid()) {
                $this->getEntityManager()->flush();
    
                $this->flashMessenger()->addMessage('Record updated.');
                
                // Redirect to list of records
                return $this->redirect()->toRoute('retext');
            }
        }
    
        return array(
                'id' => $id,
                'form' => $form,
        );
    }
    
    public function deleteAction()
    {
        $this->_checkStatus();
        
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('retext');
        }
    
        $request = $this->getRequest();
        if ($request->isPost()) {
            $del = $request->getPost('del', 'No');
    
            if ($del == 'Yes') {
                $id = (int) $request->getPost('id');
                $record = $this->getEntityManager()->find('Retext\Entity\Retext', $id);
                if ($record) {
                    $this->getEntityManager()->remove($record);
                    $this->getEntityManager()->flush();
                    $this->flashMessenger()->addMessage('Record deleted.');
                }
            }
            
            // Redirect to list of records
            return $this->redirect()->toRoute('retext');
        }
    
        return array(
                'id'    => $id,
                'record' => $this->getEntityManager()->find('Retext\Entity\Retext', $id)
        );
    }   

    
    /**
     * Check if user is authenticated
     */
    private function _checkStatus()
    {
        if (!$this->zfcUserAuthentication()->hasIdentity()) {
            return $this->redirect()->toRoute('home');
        }        
    }    
}
