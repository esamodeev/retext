<?php
namespace Retext\Form;

use Zend\Form\Form;
use DoctrineModule\Validator\NoObjectExists as NoObjectExistsValidator;
use Doctrine\ORM\EntityManager;

class RetextForm extends Form
{
    /**
     * @var EntityManager
     */
    protected $em;
    
    public function __construct(EntityManager $em, $name = null, $options = array())
    {
        $this->em = $em;
        
        parent::__construct('record-form');

        $this->add(array(
                'name' => 'id',
                'type' => 'Hidden',
        ));
        $this->add(array(
                'name' => 'keyword',
                'type' => 'Text',
                'options' => array(
                        'label' => 'Keyword',
                ),
                'attributes' => array(
                        'required'  => 'required'
                )                
        ));
        $this->add(array(
                'name' => 'response',
                'type' => 'Text',
                'options' => array(
                        'label' => 'Response text (URL)',
                ),
                'attributes' => array(
                        'required'  => 'required'
                )                
        ));
        $this->add(array(
                'name' => 'submit',
                'type' => 'Submit',
                'attributes' => array(
                        'value' => 'Go',
                        'id' => 'submitbutton',
                ),
        ));
    }
}