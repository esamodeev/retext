<?php
namespace Retext\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use DoctrineModule\Validator\NoObjectExists as NoObjectExistsValidator;
use Doctrine\ORM\EntityManager;

/**
 * Retext.
 *
 * @ORM\Entity
 * @ORM\Table(name="retext")
 * @property string $keyword
 * @property string $response
 * @property int $id
 */
class Retext implements InputFilterAwareInterface
{
    protected $inputFilter;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer");
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $keyword;

    /**
     * @ORM\Column(type="string", unique=true)
     */
    protected $response;

    /**
     * @var EntityManager
     */
    protected $_em;

    
    public function __construct(EntityManager $em)
    {
        $this->_em = $em;
    }
    
        
    /**
     * Magic getter to expose protected properties.
     *
     * @param string $property
     * @return mixed
     */
    public function __get($property)
    {
        return $this->$property;
    }

    /**
     * Magic setter to save protected properties.
     *
     * @param string $property
     * @param mixed $value
     */
    public function __set($property, $value)
    {
        $this->$property = $value;
    }

    /**
     * Convert the object to an array.
     *
     * @return array
     */
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    /**
     * Populate from an array.
     *
     * @param array $data
     */
    public function exchangeArray  ($data = array())
    {
        $this->id = $data['id'];
        $this->keyword = $data['keyword'];
        $this->response = $data['response'];
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
       
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
    
            $inputFilter->add(array(
                    'name'     => 'id',
                    'required' => true,
                    'filters'  => array(
                            array('name' => 'Int'),
                    ),
            ));
            
            
            $filterArray = array(
                    'name'     => 'keyword',
                    'required' => true,
                    'filters'  => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                            array(
                                    'name'    => 'StringLength',
                                    'options' => array(
                                            'encoding' => 'UTF-8',
                                            'min'      => 1,
                                            'max'      => 100,
                                    ),
                            ),                                                        
                    ),
            );
            
            if (!empty($this->_em))
            {
                $filterArray['validators'][] = array(
                        'name' => 'DoctrineModule\Validator\NoObjectExists',
                        'options' => array(
                                'object_repository' => $this->_em->getRepository('Retext\Entity\Retext'),
                                'fields' => 'keyword',
                                'messages' => array(
                                        'objectFound' => 'This keyword already exists!'
                                )
                        )
                );
            }
            
            $inputFilter->add($filterArray);
            

            $inputFilter->add(array(
                    'name'     => 'response',
                    'required' => true,
                    'filters'  => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                            array(
                                    'name'    => 'StringLength',
                                    'options' => array(
                                            'encoding' => 'UTF-8',
                                            'min'      => 1,
                                            'max'      => 200,
                                    ),
                            ),
                            array(
                                    'name'    => 'Uri',
                                    'options' => array(
                                            'allowRelative' => false,
                                    ),
                            ),                            
                    ),
            ));
    
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }
}
